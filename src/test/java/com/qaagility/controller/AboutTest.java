package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class AboutTest {

    @Test
    public void testDescription() {
        About about = new About();

        // Test the description method
        String result = about.desc();
        String expected = "This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!";
        assertEquals("Description", expected, result);
    }
}