package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CntTest {

    @Test
    public void testDivisionByNonZeroDenominator() {
        Cnt cnt = new Cnt();

        // Test division by non-zero denominator
        int result = cnt.d(10, 2);
        assertEquals("Division by non-zero denominator", 5, result);
    }

    @Test
    public void testDivisionByZero() {
        Cnt cnt = new Cnt();

        // Test division by zero (should return Integer.MAX_VALUE)
        int result = cnt.d(10, 0);
        assertEquals("Division by zero", Integer.MAX_VALUE, result);
    }
}